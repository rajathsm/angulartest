import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MatSliderModule } from '@angular/material/slider';
import { NgxSpinnerService } from "ngx-spinner";
//import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fbTestF';
  items: Observable<any[]>;
  totalAngularPackages;
  noPlate:string;
  urlsent:string='http://google.com';
  postId;
  url='http://localhost:5000/NumberPlateDetectionEP';
  //url='https://rajath.pagekite.me/NumberPlateDetectionEP'
  postData = {
    'url': 'https://firebasestorage.googleapis.com/v0/b/testing-cd327.appspot.com/o/test%2F1575464869429_plate%202.JPG?alt=media&token=e95069b8-6aa7-44bc-b741-76f4fe4ff64b'
  };
  json;
  constructor(db: AngularFirestore,private http: HttpClient,private spinner: NgxSpinnerService) {
    this.items = db.collection('test').valueChanges();
    
  }

  // getUsers(){
  //   return this.http.get('http://jsonplaceholder.typicode.com/users')
  //   .pipe(
  //       res => res
        
  //   );

    
//}



  ngOnInit(){
    //this.getUsers();
  //   this.http.get<any>('http://localhost:5000').subscribe(data => {
  //     this.totalAngularPackages = data;
  //     console.log(this.totalAngularPackages);
  // })
  this.spinner.show();

  this.http.post(this.url, this.postData).toPromise().then((data:any) => {
    
    this.spinner.hide();
    console.log(data.CreditsUsage);
    this.noPlate=data.NumberPlate;
    //console.log(data.json.test);
    //this.json = JSON.stringify(data.json);
  });
  }
  

//   this.http.post<any>('http://localhost:5000/post',this.postData ).subscribe(data => {
//     this.spinner.show();
//     this.postId = data;
//     console.log(this.postId);
// })
  


   
}
